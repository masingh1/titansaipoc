package com.POC.AIBackend.util;

import com.POC.AIBackend.configuration.SummaryConstants;
import com.POC.AIBackend.model.Message;
import com.POC.AIBackend.model.OpenAIRequest;
import com.POC.AIBackend.model.OpenAIResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static com.POC.AIBackend.configuration.SummaryConstants.*;

@Component
@Slf4j
public class



OpenAIUtil {
    @Qualifier("openaiRestTemplate")
    @Autowired
    private RestTemplate restTemplate;

    @Value("${openai.api.url}")
    private String apiUrl;

    public String generateMessage(String subject,String prompt,double temp){
        List<Message> initialMessage = new ArrayList<>();
        initialMessage.add(Message.builder().role(systemRole).content(systemMessage).build());
        initialMessage.add(Message.builder().role(userRole).content(prompt).build());
        log.info(prompt);
        // create a request
        OpenAIRequest request = OpenAIRequest.builder()
                .messages(initialMessage)
                .n(varN)
                .temperature(temp)
                .model(fetchModel(subject))
                .build();

        // call the API
        log.info("Sending response to open AI .." + request);
        OpenAIResponse response = null;
        try {
            response = restTemplate.postForObject(apiUrl, request, OpenAIResponse.class);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        if (response == null || response.getChoices() == null || response.getChoices().isEmpty()) {
            return "No response";
        }

       /* try {
            updateNumbersInFile(response.getUsage().get(0).getCompletion_tokens(), response.getUsage().get(0).getPrompt_tokens(), response.getUsage().get(0).getTotal_tokens());
        }
        catch (Exception e){
            log.error(e.getMessage());
        }*/
        //log.info("Token data: " + response.getUsage());
        log.info(
                "The generated response is:\n" + response.getChoices().get(0).getMessage().getContent());
        // return the first response
        return response.getChoices().get(0).getMessage().getContent();
    }

    public String fetchModel(String subject){
        if(subject.equals("Maths"))return mathsModel;
        else return defaultModel;
    }

}
