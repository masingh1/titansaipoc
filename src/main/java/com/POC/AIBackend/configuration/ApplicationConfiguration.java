package com.POC.AIBackend.configuration;

import org.springframework.context.annotation.Configuration;

/** Main application Spring configuration */
@Configuration
public class ApplicationConfiguration {}
