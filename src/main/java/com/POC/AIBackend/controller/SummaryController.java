package com.POC.AIBackend.controller;

import com.POC.AIBackend.model.SummarizedResult;
import com.POC.AIBackend.model.SummaryRequest;
import com.POC.AIBackend.service.SummaryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class SummaryController {

    @Autowired
    SummaryService summaryService;

    @PostMapping(path = "/createPersonalisedAnswer", consumes = "application/json", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:63342")
    public SummarizedResult summaryApi(
            @Validated @RequestBody SummaryRequest request) {
        SummarizedResult result = summaryService.generateSummary(request);
        return result;
    }
}
