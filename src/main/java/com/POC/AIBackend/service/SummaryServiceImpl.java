package com.POC.AIBackend.service;

import com.POC.AIBackend.model.ModelCreativity;
import com.POC.AIBackend.model.SummarizedResult;
import com.POC.AIBackend.model.SummaryRequest;
import com.POC.AIBackend.util.OpenAIUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.POC.AIBackend.configuration.SummaryConstants.*;

@Service
@Slf4j
public class SummaryServiceImpl implements SummaryService {

    @Autowired
    OpenAIUtil openAIUtil;
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public SummarizedResult generateSummary(SummaryRequest request) {
        double temp = balancedTemp;
        if (request.getModelCreativity().equals(ModelCreativity.CREATIVE)) temp = creativeTemp;
        else if (request.getModelCreativity().equals(ModelCreativity.DETERMINISTIC)) temp = deterministicTemp;
        try {
            return objectMapper.readValue(openAIUtil.generateMessage(request.getSubject(),generatePrompt(request),temp), SummarizedResult.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private String generatePrompt(SummaryRequest request) {
        return promptPrefix + request.getHobbies() + promptContext + request.getMajors() + promptCity + request.getCity() + promptSuffix+request.getAnswerDifficulty()+questionPrompt + request.getQuestion();
    }
}
