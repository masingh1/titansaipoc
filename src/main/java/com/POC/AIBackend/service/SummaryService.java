package com.POC.AIBackend.service;

import com.POC.AIBackend.model.SummarizedResult;
import com.POC.AIBackend.model.SummaryRequest;

public interface SummaryService {
    SummarizedResult generateSummary(SummaryRequest request);
}
