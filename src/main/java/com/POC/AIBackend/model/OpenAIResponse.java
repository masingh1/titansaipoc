package com.POC.AIBackend.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class OpenAIResponse {
    @JsonProperty("choices")
    private List<Choice> choices;

    @Data
    public static class Choice {
        @JsonProperty("index")
        private int index;

        @JsonProperty("message")
        private Message message;
    }
}

