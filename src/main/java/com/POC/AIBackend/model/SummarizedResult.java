package com.POC.AIBackend.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SummarizedResult {
    @JsonProperty("answer")
    private String answer;

    @JsonProperty("personalisedAnswer")
    private String personalisedAnswer;
    public SummarizedResult() {
    }
    public SummarizedResult(String answer, String personalisedAnswer) {
        this.answer = answer;
        this.personalisedAnswer = personalisedAnswer;
    }

}
