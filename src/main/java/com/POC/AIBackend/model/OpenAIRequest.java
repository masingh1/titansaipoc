package com.POC.AIBackend.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class OpenAIRequest {
  @JsonProperty("model")
  private String model;

  @JsonProperty("messages")
  private List<Message> messages;

  @JsonProperty("n")
  private int n;

  @JsonProperty("temperature")
  private double temperature;

  // Default constructor for deserialization
  public OpenAIRequest() {
  }

  // All-args constructor
  public OpenAIRequest(String model, List<Message> messages, int n, double temperature) {
    this.model = model;
    this.messages = messages;
    this.n = n;
    this.temperature = temperature;
  }
}