package com.POC.AIBackend.model;

import lombok.Data;

import java.util.List;

@Data
public class SummaryRequest {

    private String subject;
    private List<String> hobbies;
    private List<String> majors;
    private AnswerDifficulty answerDifficulty;
    private String city;
    private ModelCreativity modelCreativity;
    private String question;

}
