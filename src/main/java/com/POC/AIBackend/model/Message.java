package com.POC.AIBackend.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Message {
  @JsonProperty("role")
  private String role;

  @JsonProperty("content")
  private String content;

  // Default constructor for deserialization
  public Message() {
  }

  // All-args constructor
  public Message(String role, String content) {
    this.role = role;
    this.content = content;
  }
}
