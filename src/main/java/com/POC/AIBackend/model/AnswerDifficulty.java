package com.POC.AIBackend.model;

public enum AnswerDifficulty {
    Fourth_Grader,
    HighSchool_Student,
    College_Student,
    Researcher
}
