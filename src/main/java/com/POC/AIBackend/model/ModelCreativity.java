package com.POC.AIBackend.model;

public enum ModelCreativity {
    DETERMINISTIC,
    BALANCED,
    CREATIVE;

}
